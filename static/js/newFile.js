//	TODO(Stipic) : change.js IN DEVELOPMENT
app.controller("newfileController", function ($scope, $http, $window) {
	$scope.changePassword = function (oldPassword, newPassword, newPassword2) {
		if (newPassword == null || newPassword2 == null) {
			swal("Error", "Passwords cannot be empty!", "error")
		} else {
			if (newPassword == newPassword2) {
				if (newPassword.length > 5) {
					var data = {oldPassword: oldPassword, newPassword: newPassword};
					$http({
						method: "POST",
						url: "/api/change",
						data: data
					}).then(function successCallback(response) {
						if (response.data == "924") {
							swal("Error", "You entered wrong password!", "error")
						} else if (response.data == "success") {
							swal({
								title: "Success",
								text: "You have successfully changed your password!",
								type: "success"
							}, function () {
								$window.location.href = "/";
							});
						}
					}, function errorCallback(response) {
						console.log("ERROR", response);
					});
				} else {
					swal("Error", "Password must be at least 6 characters long!", "error");
				}
			} else {
				swal("Error", "New passwords do not match!", "error");
			}
		}
	}
});