var theme = "blue";


function setTheme (theme) {
	if (theme == "blue") {
		$(".link").css("color", "#515FB4");
		$(".link").hover().css("color", "#3D4A98");
		$(".tempTheme").addClass("blue");
		$(".hidden-border").addClass("blue-border");
	}
}

function acceptCookies() {
	$.ajax({
		type: "POST",
		url: "/api/acceptCookies"
	});
	document.getElementById("acceptedCookies").style.display = "none";
}
