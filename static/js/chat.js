app.controller("chatController", function ($rootScope, $scope, $http) {

	$scope.loadedRoom = function (room, iv) {
		var data = {room: room};
		$http({
			method: "POST",
			url: "/api/getRoomMessages",
			data: data
		}).then(function successCallback(response) {
			var messages = response.data;
			for (var i = 0; i < messages.length; i++) {
				messages[i]["message"] = CryptoJS.AES.decrypt(messages[i]["message"], room, {iv: iv}).toString(CryptoJS.enc.Utf8);
				var sender = messages[i]["message"].substring(0, messages[i]["message"].indexOf(":"));
				messages[i]["message"] = messages[i]["message"].substring(messages[i]["message"].indexOf(":") + 2);
				var zero1 = "";
				var zero2 = "";
				var zero3 = "";
				var year = new Date(messages[i]["datetimes"] + " UTC").getFullYear();
				var month = new Date(messages[i]["datetimes"] + " UTC").getUTCMonth() + 1;
				var day = new Date(messages[i]["datetimes"] + " UTC").getDate();
				var hours = new Date(messages[i]["datetimes"] + " UTC").getHours();
				var minutes = new Date(messages[i]["datetimes"] + " UTC").getMinutes();
				var seconds = new Date(messages[i]["datetimes"] + " UTC").getSeconds();
				if (hours < 10) zero1 = "0";
				if (minutes < 10) zero2 = "0";
				if (seconds < 10) zero3 = "0";
				var myDate = day + "/" + month + "/" + year + " " + zero1 + hours + ":" + zero2 + minutes + ":" + zero3 + seconds;
				if (sender == $rootScope.username) {
					$("#messages").append("<div class='message'><li class=\"ChatLog__entry ChatLog__entry_mine\"><p class=\"ChatLog__message\">" + messages[i]["message"] + "</p></li><small class=\"sentDate sentDateMine\">" + myDate + "</small></div>");
				} else {
					$("#messages").append("<div class='message'><li class=\"ChatLog__entry\"><span class=\"padding\">" + sender + ": </span><p class=\"ChatLog__message\">" + messages[i]["message"] + "</p></li><small class='sentDate'>" + myDate + "</small></div>");
				}
			}
			window.scrollTo(0,document.body.scrollHeight);
			
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.mobileDropdown = function () {
		var visibility = document.getElementById("messages").style.visibility;
		if (visibility == "hidden") document.getElementById("messages").style.visibility = "visible";
		else document.getElementById("messages").style.visibility = "hidden";
	}
});