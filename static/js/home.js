app.controller("homeController", function ($scope, $http) {
	var self = this;
	self.files = [];
	self.folders = [];
	self.filesInFolder = [];
	self.inFolders = [];
	self.appendHtml = [];
	$http({
		method: "POST",
		url: "/api/returnList"
	}).then(function successCallback(response) {
		for (var i = 0; i < response.data.length; i++) {
			var tmp = response.data[i];
			var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"], value: tmp["value"]};
			/* if (file.type == "file") {
				var inFolder = 0;
				console.log(self.filesInFolder.length)
				for (var j = 0; j < self.filesInFolder.length; j++) {
					console.log(self.filesInFolder[j])
					if (self.filesInFolder[j].indexOf(file.unique_id + ",") > -1) {
						inFolder = 1;
					}
				}
				if (inFolder == 0) {
					self.files.push(file);
				}
			} else {
				self.folders.push(file);
				self.filesInFolder.push(file.value);
			} */
			if (file.type == "folder") {
				self.folders.push(file);
				self.filesInFolder.push(file.value);
			}
		}
		for (var i = 0; i < response.data.length; i++) {
			var tmp = response.data[i];
			var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"], value: tmp["value"]};

			if (file.type == "file") {
				var inFolder = 0;
				for (var j = 0; j < self.filesInFolder.length; j++) {
					if (self.filesInFolder[j].indexOf(file.unique_id + ",") > -1) {
						inFolder = 1;
					}
				}
				if (inFolder == 0) {
					self.files.push(file);
				} else {
					self.inFolders.push(file);
				}
			}
		}

		/*			*/
		//console.log(self.filesInFolder);
		for (var i = 0; i < self.folders.length; i++) {
			for (var j = 0; j < response.data.length; j++) {
				var tmp = response.data[j];
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"]};

				if (self.filesInFolder[i].indexOf(file.unique_id + ",") > -1) {
					//$("#folder" + i).append('<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="btn btn-xs btn-default" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove" style="color: #1ab188" aria-hidden="true"></span></span></span></div>');
					//$("#folder" + i).html('<div class="list-group-item item">Hello</div>');
					var oldValue = self.appendHtml[i];
					//self.appendHtml[i] = oldValue + '<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="btn btn-xs btn-default" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove" style="color: #1ab188" aria-hidden="true"></span></span></span></div>';
					self.appendHtml[i] = oldValue + '<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="pull-right"><div class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-option-vertical" style="color: #1ab188"></span></a><span class="dropdown-menu width10vw pull-right"><span class="marginLeft2vw"><a class="red" href="#" onclick="deleteFile(\'' + file.unique_id + '\')">Delete</a></span><span class="btn btn-xs btn-default pull-right right-button" onclick="deleteFile(\'' + file.unique_id + '\')"><span class="glyphicon glyphicon-trash red"></span></span><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" onclick="shareFile(\'' + file.unique_id + '\')">Share</a></span><br><ul class=divider></ul><span class=marginLeft2vw><a href=# onclick="moveFile(\'' + file.unique_id + '\', \'' + self.folders[i]["unique_id"] + '\')">Move file to...</a></span><br><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" onclick="fileInfo(\'' + file.unique_id + '\')">Info</a></span><br><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" onclick="downloadFile(\'' + file.unique_id + '\')">Download</a></span><br></span></div></span></span></div>';
					//self.appendHtml[i] = oldValue + '<ul class="dropdown-menu width10vw pull-right" role="menu" aria-labelledby="dropdownMenu"><span class="marginLeft2vw"><a class="red" href="#" ng-click="deleteFile(' + file.unique_id + ')">Delete</a></span><span class="btn btn-xs btn-default pull-right right-button" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></span><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" ng-click="shareFile(' + file.unique_id + ')">Share</a></span><br><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" ng-click="fileInfo(' + file.unique_id + ')">Info</a></span><br></ul>';
					//<ul class="dropdown-menu"></ul>
					//$("#folder" + i).append('<h1>hello</h1>' + file.unique_id);
				}
			}
		}
		$scope.fillFolders(self.appendHtml);
		document.getElementById("allFiles").style.display = "inline";
		setTimeout(function () {
			$("#loader").animate({
				top: "-100vh"
			}, 1000);
		}, 1000);
		setTimeout(function () {
			$("#allFiles").animate({
				top: "-100px"
			}, 500);
			setTimeout(function () {
				document.getElementById("loader").style.hidden = "true";
			}, 500);
		}, 950);
	}, function errorCallback(response) {
		console.log("ERROR", response)
	});

	$scope.download = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var data = decodeURIComponent(response.data.value);

			var filename = response.data.title + "." + reverseExtension(response.data.extension);

			if (!data) {
				console.error('No data');
				return;
			}

			if (!filename) {
				filename = 'download.json';
			}

			if (typeof data === 'object') {
				data = JSON.stringify(data, undefined, 2);
			}

			var blob = new Blob([data], {type: 'text/json'});

			// FOR IE:

			if (window.navigator && window.navigator.msSaveOrOpenBlob) {
				window.navigator.msSaveOrOpenBlob(blob, filename);
			} else {
				var e = document.createEvent('MouseEvents'),
				a = document.createElement('a');

				a.download = filename;
				a.href = window.URL.createObjectURL(blob);
				a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
				e.initEvent('click', true, false, window,
					0, 0, 0, 0, 0, false, false, false, false, 0, null);
				a.dispatchEvent(e);
			}


		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.shareFile = function (unique_id) {
		swal({
			title: "Share file:",
			text: '<div id="alert"> <div class="form-group"> <label for="sel1">Select permission:</label> <select class="form-control" id="sharedPermissions"> <option>Read</option> <option>Write</option> <option>Admin</option> </select></div> </div>',
			html: true,
			type: "input",
			closeOnConfirm: false,
			animation: "slide-from-top",
			showCancelButton: true,
			inputPlaceholder: "Username",
			showLoaderOnConfirm: true
		}, function(inputValue){
			if (inputValue === false) return false;

			if (inputValue === "") {
				swal.showInputError("You need to write username!");
				return false;
			}
			
			var permission = $("#sharedPermissions").val();
			var data = {username: inputValue, permission: permission, unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/shareFile",
				data: data
			}).then(function successCallback(response) {
				swal.close();
			}, function errorCallback(response) {
				console.log("ERROR", response);
				swal.close();
			});
		});
	}

	function reverseExtension (extension) {
		var newbie = "";
		if (extension == "plain_text") newbie = "txt";
		else if (extension == "c_cpp") newbie = "cpp";
		else if (extension == "javascript") newbie = "js";
		else if (extension == "perl") newbie = "pl";
		else if (extension == "python") newbie = "py";
		else newbie = extension;
		return newbie;
	}

	$scope.fillFolders = function (appendHtml) {
		fillFolders(appendHtml);
	}

	$scope.renameFolder = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var folderName = response.data["title"];
			swal({
				title: "Rename folder",
				type: "input",
				showCancelButton: true,
				closeOnConfirm: false,
				animation: "slide-from-top",
				inputValue: folderName
			},
			function(inputValue){
				if (inputValue === false) return false;
				
				if (inputValue === "") {
					swal.showInputError("You need to write something!");
					return false
				}
				
				data = {unique_id: unique_id, title: inputValue};
				$http({
					method: "POST",
					url: "/api/renameFile",
					data: data
				}).then(function successCallback(response) {
					location.reload();
				}, function errorCallback(response) {
					console.log("ERROR", response);
				});

				
			});
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.deleteFile = function (unique_id) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function () {
			var data = {unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/deleteFile",
				data: data
			}).then(function successCallback(response) {
				swal({ 
					title: "Deleted!",
					text: "Your file has been deleted.",
					type: "success" 
				}, function(){
					location.reload();
				});
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		});
	}

	$scope.moveFile = function (unique_id, currentFolder) {
		var selections = "<option value=''>/</option>";
		//<option>Read</option> <option>Write</option> <option>Admin</option>
		console.log(self.folders);

		for (var i = 0; i < self.folders.length; i++) {
			selections += "<option value='" + self.folders[i]["unique_id"] + "'>/" + self.folders[i]["title"] + "</option>"
		}
		swal({
			title: "Move file:",
			text: '<div id="alert"> <div class="form-group"> <label for="sel1">Select folder:</label> <select class="form-control" id="selectedFolder">' + selections + '</select></div> </div>',
			html: true,
			closeOnConfirm: false,
			animation: "slide-from-top",
			showCancelButton: true,
			showLoaderOnConfirm: true
		}, function(inputValue){
			if (inputValue === false) return false;

			if (inputValue === "") {
				swal.showInputError("You need to write username!");
				return false;
			}

			var selectedFolder = $("#selectedFolder").val();
			var data = {selectedFolder: selectedFolder, unique_id: unique_id, currentFolder: currentFolder};
			$http({
				method: "POST",
				url: "/api/moveFile",
				data: data
			}).then(function successCallback(response) {
				swal.close();
				location.reload();
			}, function errorCallback(response) {
				console.log("ERROR", response);
				swal.close();
			});
		});
			
	}

	$scope.deleteFolder = function (unique_id) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this folder and all files that this folder includes!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function(){
			var data = {unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/deleteFolder",
				data: data
			}).then(function successCallback(response) {
				swal({ 
					title: "Deleted!",
					text: "Your folder has been deleted.",
					type: "success" 
				}, function(){
					location.reload();
				});
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		});
	}

	$scope.fileInfo = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			console.log(response);
			var tmp = response.data;
			if (tmp["last_modified_date"]) {
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"], created_date: tmp["created_date"], last_modified_user: tmp["last_modified_user"]};
				swal({
					title: "File info:",
					text: '<style>.left { float: left; }</style><div id="alert"> <div class="form-group"> <label>Title: </label> ' + file.title + '<br><label>Extension: </label> ' + file.extension + ' <br><label>Owner: </label> ' + file.owner + ' <br><label>Created: </label> ' + file.created_date + ' <br><label>Last modified: </label> ' + file.last_modified_date + ' <br><label>Unique ID: </label> ' + file.unique_id + ' </div> </div>',
					html: true,
					animation: "slide-from-top"
				});
			}
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.folderInfo = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			console.log(response);
			var tmp = response.data;
			if (tmp["last_modified_date"]) {
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"], created_date: tmp["created_date"], last_modified_user: tmp["last_modified_user"], value: tmp["value"]};
				var numOfFiles = file["value"].split(",").length - 1;
				swal({
					title: "Folder info:",
					text: '<style>.left { float: left; }</style><div id="alert"> <div class="form-group"> <label>Name: </label> ' + file.title + '<br><label>Owner: </label> ' + file.owner + ' <br><label>Created: </label> ' + file.created_date + ' <br><label>Files in folder: </label> ' + numOfFiles + ' <br></div> </div>',
					html: true,
					animation: "slide-from-top"
				});
			}
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.openFolder = function (folderId) {
		if ($("#folder" + folderId).hasClass("hidden")){
			$("#folder" + folderId).removeClass("hidden");
			$("#arrow" + folderId).addClass("caret-reversed");
		} else {
			$("#folder" + folderId).addClass("hidden");
			$("#arrow" + folderId).removeClass("caret-reversed");
		}
		//document.getElementById("folder" + folderId).style.display = "inline";
	}

	$scope.mobileDropdown = function () {
		var visibility = document.getElementById("divList").style.visibility;
		if (visibility == "hidden") document.getElementById("divList").style.visibility = "visible";
		else document.getElementById("divList").style.visibility = "hidden";
	}
});