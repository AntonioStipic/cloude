app.controller("indexController", function ($scope, $http) {
	$scope.signin = function (username, password) {
		var data = {username: username, password: password};
		$http({
			method: "POST",
			url: "/signin",
			data: data
		}).then(function successCallback(response) {
			if (response["data"] == "success") {
				window.location.href = "/";
			} else if (response["data"] == "956") {
				swal("Oops...", "User does not exist.", "error");
			} else if (response["data"] == "924") {
				swal("Oops...", "Password you entered is incorrect.", "error");
			}
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.signup = function (username, newPassword, newPassword2, email) {
		console.log(email);
		if (newPassword == null || newPassword2 == null) {
			swal("Error", "Passwords cannot be empty!", "error")
		} else {
			if (newPassword == newPassword2) {
				if (newPassword.length > 5) {
					var data = {usernameUp: username, passwordUp: newPassword, passwordUp2: newPassword2, emailUp: email};
					$http({
						method: "POST",
						url: "/signup",
						data: data
					}).then(function successCallback(response) {
						if (response.data == "success") {
							swal({ 
								title: "Success!",
								text: "You have successfully created an account.",
								type: "success" 
							}, function(){
								window.location.href = "/";
							});
						} else if (response.data == "ER_DUP_ENTRY") {
							swal("Oops...", "Username or email are already in use.", "error");
						}
					}, function errorCallback(response) {
						console.log("ERROR", response);
					});
				} else {
					swal("Error", "Password must be at least 6 characters long!", "error");
				}
			} else {
				swal("Error", "New passwords do not match!", "error");
			}
		}
	}
});