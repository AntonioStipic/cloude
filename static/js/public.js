app.controller("publicController", function ($rootScope, $scope, $http, $window) {
	var self = this;
	self.files = [];
	
	// TODO(Djeno) : this should be GET
	$http({
		method: "POST",
		url: "/returnPublicShared"
	}).then(function successCallback(response) {
		for (var i = 0; i < response.data.length; i++) {
			var tmp = {unique_id: response.data[i]};
			var until = response.data.length;
			var yet = 0;
			$http({
				method: "POST",
				url: "/fileInfo",
				data: tmp
			}).then(function successCallback(response2) {
				var tmp = response2.data;
				yet++;
				if (tmp["last_modified_date"]) {
					var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"]};
					self.files.push(file);
				}
				if (yet == until) {
					setTimeout(function () {
						$("#loader").animate({
							top: "-100vh"
						}, 1000);
					}, 1000);
					setTimeout(function () {
						$("#divList").animate({
							top: "-100px"
						}, 500);
						setTimeout(function () {
							document.getElementById("loader").style.hidden = "true";
						}, 500);
					}, 950);
				}
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		}
	}, function errorCallback(response) {
		console.log("ERROR", response)
	});

	function reverseExtension (extension) {
		var newbie = "";
		if (extension == "plain_text") newbie = "txt";
		else if (extension == "c_cpp") newbie = "cpp";
		else if (extension == "javascript") newbie = "js";
		else if (extension == "perl") newbie = "pl";
		else if (extension == "python") newbie = "py";
		else newbie = extension;
		return newbie;
	}

	$scope.mobileDropdown = function () {
		var visibility = document.getElementById("divList").style.visibility;
		if (visibility == "hidden") document.getElementById("divList").style.visibility = "visible";
		else document.getElementById("divList").style.visibility = "hidden";
	}

	$scope.fileInfo = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var tmp = response.data;
			if (tmp["last_modified_date"]) {
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"], created_date: tmp["created_date"], last_modified_user: tmp["last_modified_user"]};
				swal({
					title: "File info:",
					text: '<style>.left { float: left; }</style><div id="alert"> <div class="form-group"> <label>Title: </label> ' + file.title + '<br><label>Extension: </label> ' + file.extension + ' <br><label>Owner: </label> ' + file.owner + ' <br><label>Created: </label> ' + file.created_date + ' <br><label>Last modified: </label> ' + file.last_modified_date + ' <br><label>Unique ID: </label> ' + file.unique_id + ' </div> </div>',
					html: true,
					animation: "slide-from-top"
				}, function(inputValue){
					if (inputValue === false) return false;

					if (inputValue === "") {
						swal.showInputError("You need to write username!");
						return false;
					}
				});
			}
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

});
