//	TODO(Djeno) : main.js (this controller is not needed)
app.controller("mainController", function ($rootScope, $scope, $http, $window) {
	//	TODO(Djeno) : Should we send ajax everytime we restart the page
	//							 	maybe save session to the $cookies
	$http({
		method: "POST",
		url: "/api/info"
	}).then(function successCallback(response) {
		$rootScope.username = response.data["username"];
		if (typeof loaded == "function") { 
			loaded(response.data["username"]);
		}
		//$scope.username = response.data["username"];
		$rootScope.sessid = response.data["sessid"];
		$rootScope.logged = true;
		if (response.data["acceptedCookies"] == 0) {
			document.getElementById("acceptedCookies").style.display = "inline";
		} else {
			document.getElementById("acceptedCookies").style.display = "none";
		}
	}, function errorCallback(response) {
		$rootScope.username = "guest";
		$rootScope.logged = false;
		console.log("ERROR", response);
	});

	$scope.IsClickEnable = 0;

	//	TODO(Djeno) : Maybe connect this to newController not to mainController
	//							 	it makes more sense
	$scope.newFolder = function () {
		swal({
			title: "New folder",
			text: "Write folder name:",
			type: "input",
			showCancelButton: true,
			closeOnConfirm: false,
			animation: "slide-from-top",
			inputPlaceholder: "Folder name"
		},
		function(inputValue){
			if (inputValue === false) return false;
			
			if (inputValue === "") {
				swal.showInputError("You need to write something!");
				return false
			}

			var data = {folderName: inputValue};
			$http({
				method: "POST",
				url: "/api/createFolder",
				data: data
			}).then(function successCallback(response) {
				$window.location.href = "/list";
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
			
		});
	}
});
