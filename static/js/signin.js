$(".form").find("input, textarea").on("keyup blur focus", function (e) {

	var $this = $(this);
	var label = $this.prev("label");

	if (e.type === "keyup") {
		if ($this.val() === "") {
			label.removeClass("highlight");
		} else {
			label.addClass("active highlight");
		}
	} else if (e.type === "blur") {
		if( $this.val() === "" ) {
			label.removeClass("active highlight"); 
		} else {
			//label.removeClass("highlight");   
		}
	} else if (e.type === "focus") {
		/* 
		if( $this.val() === "" ) {
			label.removeClass("highlight"); 
		} 
		else if( $this.val() !== "" ) {
			label.addClass("highlight");
		}
		*/
		label.addClass("active highlight");
	}

});

$(".tab a").on("click", function (e) {

	e.preventDefault();

	$(this).parent().addClass("active");
	$(this).parent().siblings().removeClass("active");

	target = $(this).attr("href");

	$(".tab-content > div").not(target).hide();

	$(target).fadeIn(600);

});

window.onload = function () {
	setTimeout(function () {
		if (document.getElementById("usernameLogin").value != "") {
			$("#usernameLogin").focus();
			$("#passwordLogin").focus()
		} else if (document.getElementById("passwordLogin").value != "") {
			$("#usernameLogin").focus();
			$("#passwordLogin").focus()
		}
	}, 100);
	//$("#passwordLogin").allchange(function () { $("#passwordLogin").focus(); });
	document.getElementById("usernameRegister").value = "";
	document.getElementById("passwordRegister").value = "";
	document.getElementById("password2Register").value = "";
}