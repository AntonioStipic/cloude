
app.controller("publicFileController", function ($rootScope, $scope, $http, $window, $location) {

	var self = this;

	$scope.data = {
	availableOptions: [
		{mode: "c_cpp", name: ".c"},
		{mode: "c_cpp", name: ".cpp"},
		{mode: "html", name: ".html"},
		{mode: "javascript", name: ".js"},
		{mode: "perl", name: ".pl"},
		{mode: "php", name: ".php"},
		{mode: "python", name: ".py"},
		{mode: "plain_text", name: ".txt"}
	]};

	var unique_id = findGetParameter("id");
	var data = {unique_id: unique_id};

	$http({
		method: "POST",
		url: "/readFile",
		data: data
	}).then(function successCallback(response) {
		$scope.response = response;
		var kod = response.data.value;
		$rootScope.kod = decodeURIComponent(kod);
		self.option = response.data.extension;
		$scope.title = response.data["title"];
		//document.getElementById("extension").innerHTML = response.data["extension"];
		editor.setValue(decodeURIComponent(response.data["value"]));
		editor.gotoLine(1);
		var searchField = "mode";
		for (var i = 0; i < $scope.data["availableOptions"].length; i++){
			if ($scope.data["availableOptions"][i][searchField] == response.data["extension"]) {
				var indexOfExtension = i + 1;
			}
		}
		document.getElementById("select").selectedIndex = indexOfExtension;
		editor.session.setMode("ace/mode/" + response.data["extension"]);
		editor.setReadOnly(true);
		document.getElementById("title").readOnly = true;
		document.getElementById("select").disabled = true;
		$scope.IsClickEnable = 0;
	}, function errorCallback(response) {
		console.log("ERROR", response)
	});

	$scope.compile = function (kod, ins) {
		$rootScope.isError = false;
		$rootScope.error = "";
		$rootScope.output = "";
		var kod = kod;
		var data = {kod : kod, ins:ins};
    

		swal({
			title: "Compiling...",
			text: 'Please wait...',
			showConfirmButton: false,
			confirmButtonColor: "#5cb85c"
		}, $http({
			data : data,
			method: "POST",
			url: "/compile"
		}).then(function successCallback(response) {
			swal({
				title: "Compilation done!",
				confirmButtonColor: "#5cb85c",
				timer: 2000
			});
			$rootScope.error = response.data.error;
			if(response.data.error == "") $rootScope.output = response.data.output;
			else {
				$rootScope.output = "";
				$rootScope.isError = true;
			}
		}), function errorCallback(response) {
			console.log("GRESKA");
			$rootScope.error = response.data.error;
		});
	}

	/* $("form").dblclick(function () {
		$(this).find('input,select').removeProp('disabled').removeClass('no-pointer');
	}).find(':input').addClass('no-pointer'); */

	$scope.extensionChanged = function () {
		var extension = $('select[name="extension"]').val();
		if (extension == "Extension") extension = "plain_text";
		editor.session.setMode("ace/mode/" + extension);
	}

	$scope.updateFile = function (jsEditor) {
		var title = $("#title").val();
		var extension = $('select[name="extension"]').val();
		if (jsEditor == 1) var value = encodeURIComponent(editor.getValue());
		else var value = encodeURIComponent(document.getElementById("textarea").value);
		if (title && extension != "? object:null ?") {
			var data = {title: title, extension: extension, value: value, unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/updateFile",
				data: data
			}).then(function successCallback(response) {
				var unique_id = response.data["id"];
				$window.location.href = "/file?id=" + unique_id;
			}, function errorCallback(response) {
				console.log("ERROR", response)
			});
		} else {
			swal("Oops...", "Title and file extension cannot be empty!", "error");
		}
	}

	$scope.shareFile = function () {
		swal({
			title: "Share file:",
			text: '<div id="alert"> <div class="form-group"> <label for="sel1">Select permission:</label> <select class="form-control" id="sharedPermissions"> <option>Read</option> <option>Write</option> <option>Admin</option> </select></div> </div>',
			html: true,
			type: "input",
			closeOnConfirm: false,
			animation: "slide-from-top",
			showCancelButton: true,
			inputPlaceholder: "Username",
			showLoaderOnConfirm: true
		}, function(inputValue){
			if (inputValue === false) return false;

			if (inputValue === "") {
				swal.showInputError("You need to write username!");
				return false;
			}
			
			var unique_id = findGetParameter("id");
			var permission = $("#sharedPermissions").val();
			var data = {username: inputValue, permission: permission, unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/shareFile",
				data: data
			}).then(function successCallback(response) {
				swal.close();
			}, function errorCallback(response) {
				console.log("ERROR", response);
				swal.close();
			});
		});
	}

	$scope.deleteFile = function () {
		var unique_id = findGetParameter("id");
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function(){
			var data = {unique_id: unique_id};
			$http({
				method: "POST",
				url: "/api/deleteFile",
				data: data
			}).then(function successCallback(response) {
				swal({ 
					title: "Deleted!",
					text: "Your file has been deleted.",
					type: "success" 
				}, function(){
					$window.location.href = "/list";
				});
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		});
	}

});