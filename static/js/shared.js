app.controller("sharedController", function ($scope, $http, $window) {
	var self = this;
	self.files = [];
	self.folders = [];
	self.filesInFolder = "";
	self.inFolders = [];
	self.appendHtml = [];
	self.loaded = 0;
	$http({
		method: "POST",
		url: "/api/returnShared"
	}).then(function successCallback(response) {
		/* for (var i = 0; i < response.data.length; i++) {
			var tmp = response.data[i];
			var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"].substring(0, 10), extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"]};
			self.files.push(file);
		}*/
		//console.log(response);
		for (var i = 0; i < response.data.length; i++) {
			var tmp = {unique_id: response.data[i]};
			$http({
				method: "POST",
				url: "/api/fileInfo",
				data: tmp
			}).then(function successCallback(response1) {
				var tmp = response1.data;
				if (tmp["type"] == "folder") {
					var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"]};
					self.folders.push(file);

					var tmpFiles = tmp["value"].split(",");
					for (var e = 0; e < tmpFiles; e++) {
						var data = {unique_id: tmpFiles[i]};
						$http({
							method: "POST",
							url: "/api/fileInfo",
							data: data
						}).then(function successCallback(response2) {
							var tmp = response2.data;
							var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"], value: tmp["value"]};
							if (file.type == "file") {
								self.files.push(file);
							}
						}, function errorCallback(response) {
							console.log("ERROR", response);
						});
					}
					if (self.loaded == 0 && self.folders.length > -1) {
						document.getElementById("allFiles").style.display = "inline";
						setTimeout(function () {
							$("#loader").animate({
								top: "-100vh"
							}, 1000);
						}, 1000);
						setTimeout(function () {
							$("#allFiles").animate({
								top: "-100px"
							}, 500);
							setTimeout(function () {
								document.getElementById("loader").style.hidden = "true";
							}, 500);
						}, 950);
					} else self.loaded = self.loaded + 1;
				}



			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		}

		for (var i = 0; i < response.data.length; i++) {
			//console.log(response.data[i]);
			var data = {unique_id: response.data[i]};
			$http({
				method: "POST",
				url: "/api/fileInfo",
				data: data
			}).then(function successCallback(response2) {
				var tmp = response2.data;
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"], value: tmp["value"]};

				if (file.type == "file") {
					self.files.push(file);
				}
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		}
		if (self.loaded == 0 && self.folders.length > -1) {
			document.getElementById("allFiles").style.display = "inline";
			setTimeout(function () {
				$("#loader").animate({
					top: "-100vh"
				}, 1000);
			}, 1000);
			setTimeout(function () {
				$("#allFiles").animate({
					top: "-100px"
				}, 500);
				setTimeout(function () {
					document.getElementById("loader").style.hidden = "true";
				}, 500);
			}, 950);
		} else self.loaded = self.loaded + 1;

		/*			*/
		//console.log(self.filesInFolder);
		/* for (var i = 0; i < self.folders.length; i++) {
			for (var j = 0; j < response.data.length; j++) {
				var tmp = response.data[j];
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"].substring(0, 10), extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], type: tmp["type"]};

				if (self.filesInFolder[i].indexOf(file.unique_id + ",") > -1) {
					//$("#folder" + i).append('<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="btn btn-xs btn-default" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove" style="color: #1ab188" aria-hidden="true"></span></span></span></div>');
					//$("#folder" + i).html('<div class="list-group-item item">Hello</div>');
					var oldValue = self.appendHtml[i];
					//self.appendHtml[i] = oldValue + '<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="btn btn-xs btn-default" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove" style="color: #1ab188" aria-hidden="true"></span></span></span></div>';
					self.appendHtml[i] = oldValue + '<div class="list-group-item item"><a href="/file?id=' + file.unique_id + '" class="link">' + file.title + '.' + file.extension + '</a><span class="pull-right"><span class="pull-right"><div class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-option-vertical" style="color: #1ab188"></span></a><span class="dropdown-menu width10vw pull-right"><span class="marginLeft2vw"><a class="red" href="#" onclick="deleteFile(\'' + file.unique_id + '\')">Delete</a></span><span class="btn btn-xs btn-default pull-right right-button" onclick="deleteFile(\'' + file.unique_id + '\')"><span class="glyphicon glyphicon-remove red"></span></span><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" onclick="shareFile(\'' + file.unique_id + '\')">Share</a></span><br><ul class=divider></ul><span class=marginLeft2vw><a href=# onclick="moveFile(\'' + file.unique_id + '\', \'' + self.folders[i]["unique_id"] + '\')">Move file to...</a></span><br><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" onclick="fileInfo(\'' + file.unique_id + '\')">Info</a></span><br></span></div></span></span></div>';
					//self.appendHtml[i] = oldValue + '<ul class="dropdown-menu width10vw pull-right" role="menu" aria-labelledby="dropdownMenu"><span class="marginLeft2vw"><a class="red" href="#" ng-click="deleteFile(' + file.unique_id + ')">Delete</a></span><span class="btn btn-xs btn-default pull-right right-button" ng-click="deleteFile(' + file.unique_id + ')"><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></span><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" ng-click="shareFile(' + file.unique_id + ')">Share</a></span><br><ul class="divider"></ul><span class="marginLeft2vw"><a href="#" ng-click="fileInfo(' + file.unique_id + ')">Info</a></span><br></ul>';
					//<ul class="dropdown-menu"></ul>
					//$("#folder" + i).append('<h1>hello</h1>' + file.unique_id);
				}
			}
		} */

		$scope.fillFolders(self.appendHtml);
	}, function errorCallback(response) {
		console.log("ERROR", response)
	});

	$scope.download = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var data = decodeURIComponent(response.data.value);

			var filename = response.data.title + "." + reverseExtension(response.data.extension);

			if (!data) {
				console.error('No data');
				return;
			}

			if (!filename) {
				filename = 'download.json';
			}

			if (typeof data === 'object') {
				data = JSON.stringify(data, undefined, 2);
			}

			var blob = new Blob([data], {type: 'text/json'});

			// FOR IE:

			if (window.navigator && window.navigator.msSaveOrOpenBlob) {
				window.navigator.msSaveOrOpenBlob(blob, filename);
			} else {
				var e = document.createEvent('MouseEvents'),
				a = document.createElement('a');

				a.download = filename;
				a.href = window.URL.createObjectURL(blob);
				a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
				e.initEvent('click', true, false, window,
					0, 0, 0, 0, 0, false, false, false, false, 0, null);
				a.dispatchEvent(e);
			}


		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	function reverseExtension (extension) {
		var newbie = "";
		if (extension == "plain_text") newbie = "txt";
		else if (extension == "c_cpp") newbie = "cpp";
		else if (extension == "javascript") newbie = "js";
		else if (extension == "perl") newbie = "pl";
		else if (extension == "python") newbie = "py";
		else newbie = extension;
		return newbie;
	}

	$scope.download = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var data = decodeURIComponent(response.data.value);

			var filename = response.data.title + "." + reverseExtension(response.data.extension);

			if (!data) {
				console.error('No data');
				return;
			}

			if (!filename) {
				filename = 'download.json';
			}

			if (typeof data === 'object') {
				data = JSON.stringify(data, undefined, 2);
			}

			var blob = new Blob([data], {type: 'text/json'});

			// FOR IE:

			if (window.navigator && window.navigator.msSaveOrOpenBlob) {
				window.navigator.msSaveOrOpenBlob(blob, filename);
			} else {
				var e = document.createEvent('MouseEvents'),
				a = document.createElement('a');

				a.download = filename;
				a.href = window.URL.createObjectURL(blob);
				a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
				e.initEvent('click', true, false, window,
					0, 0, 0, 0, 0, false, false, false, false, 0, null);
				a.dispatchEvent(e);
			}


		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.fillFolders = function (appendHtml) {
		fillFolders(appendHtml);
	}

	$scope.mobileDropdown = function () {
		var visibility = document.getElementById("divList").style.visibility;
		if (visibility == "hidden") document.getElementById("divList").style.visibility = "visible";
		else document.getElementById("divList").style.visibility = "hidden";
	}

	$scope.fileInfo = function (unique_id) {
		var tmp = {unique_id: unique_id};
		$http({
			method: "POST",
			url: "/api/fileInfo",
			data: tmp
		}).then(function successCallback(response) {
			var tmp = response.data;
			if (tmp["last_modified_date"]) {
				var file = {title: tmp["title"], last_modified_date: tmp["last_modified_date"], extension: reverseExtension(tmp["extension"]), unique_id: tmp["unique_id"], owner: tmp["owner"], created_date: tmp["created_date"], last_modified_user: tmp["last_modified_user"]};
				swal({
					title: "File info:",
					text: '<style>.left { float: left; }</style><div id="alert"> <div class="form-group"> <label>Title: </label> ' + file.title + '<br><label>Extension: </label> ' + file.extension + ' <br><label>Owner: </label> ' + file.owner + ' <br><label>Created: </label> ' + file.created_date + ' <br><label>Last modified: </label> ' + file.last_modified_date + ' <br><label>Unique ID: </label> ' + file.unique_id + ' </div> </div>',
					html: true,
					animation: "slide-from-top"
				}, function(inputValue){
					if (inputValue === false) return false;

					if (inputValue === "") {
						swal.showInputError("You need to write username!");
						return false;
					}
				});
			}
		}, function errorCallback(response) {
			console.log("ERROR", response);
		});
	}

	$scope.openFolder = function (folderId) {
		if ($("#folder" + folderId).hasClass("hidden")){
			$("#folder" + folderId).removeClass("hidden");
			$("#arrow" + folderId).addClass("caret-reversed");
		} else {
			$("#folder" + folderId).addClass("hidden");
			$("#arrow" + folderId).removeClass("caret-reversed");
		}
		//document.getElementById("folder" + folderId).style.display = "inline";
	}


	$scope.unshareFile = function (unique_id) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to view this file anymore!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		}, function () {
			var tmp = {unique_id: unique_id};

			$http({
				method: "POST",
				url: "/api/unshareFile",
				data: tmp
			}).then(function successCallback(response) {
				location.reload();
			}, function errorCallback(response) {
				console.log("ERROR", response);
			});
		});
	}
});

