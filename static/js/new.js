app.controller("newController", function ($scope, $http, $window) {
	$scope.data = {
	availableOptions: [
		{mode: "c_cpp", name: ".c"},
		{mode: "c_cpp", name: ".cpp"},
		{mode: "css", name: ".css"},
		{mode: "html", name: ".html"},
		{mode: "java", name: ".java"},
		{mode: "javascript", name: ".js"},
		{mode: "perl", name: ".pl"},
		{mode: "php", name: ".php"},
		{mode: "python", name: ".py"},
		{mode: "plain_text", name: ".txt"}
	]};

	$http({
		method: "POST",
		url: "/api/preferencesInfo"
	}).then(function successCallback(response) {
		editor.setTheme("ace/theme/" + response.data.editorTheme);
	}, function errorCallback(response) {
		console.log("ERROR", response)
	});

	$scope.extensionChanged = function () {
		var extension = $('select[name="extension"]').val();
		if (extension == "Extension") extension = "plain_text";
		editor.session.setMode("ace/mode/" + extension);
	}

	$scope.createFile = function () {
		var title = $("#title").val();
		var extension = $('select[name="extension"]').val();
		var value = encodeURIComponent(editor.getValue());
		if (title && extension != "? object:null ?") {
			var data = {title: title, extension: extension, value: value};
			$http({
				method: "POST",
				url: "/api/createFile",
				data: data
			}).then(function successCallback(response) {
				var unique_id = response.data["id"];
				$window.location.href = "/file?id=" + unique_id;
			}, function errorCallback(response) {
				console.log("ERROR", response)
			});
		} else {
			swal("Oops...", "Title and file extension cannot be empty!", "error");
		}
	}

});
