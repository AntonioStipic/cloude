// TODO(Sale/Antonio) : Error in /newhtml

app.controller("preferencesController", function ($rootScope, $scope, $http) {

	$http({
		method: "POST",
		url: "/api/preferencesInfo"
	}).then(function successCallback(response) {
		if (response.data.jsEditor == 1) {
			 $("#jsEditorToggle").bootstrapToggle("on");
		} else {
			$("#jsEditorToggle").bootstrapToggle("off");
		}
		document.getElementById("editorThemeSelect").value = response.data.editorTheme;
	}, function errorCallback(response) {
		console.log("ERROR", response);
	});

});

function twoDigits(d) {
	if(0 <= d && d < 10) return "0" + d.toString();
	if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	return d.toString();
}

Date.prototype.toMysqlFormat = function() {
	return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

function findGetParameter (parameterName) {
	var result = null,
	tmp = [];
	location.search
		.substr(1)
		.split("&")
		.forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		});
	return result;
}
