-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2017 at 12:38 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cloudeDatabase`
--
CREATE DATABASE IF NOT EXISTS `cloudeDatabase` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `cloudeDatabase`;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `unique_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `users` text COLLATE utf8_unicode_ci NOT NULL,
  `last_modified_date` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` text COLLATE utf8_unicode_ci NOT NULL,
  `last_modified_user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `owner` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `title`, `extension`, `value`, `unique_id`, `users`, `last_modified_date`, `created_date`, `last_modified_user`, `owner`) VALUES
(1, 'test', 'plain_text', 'Is%20it%20working%3F', '3MefNgOQitJecCYi1sh1ov4S2w5JguDF', '{"owner":"A","admin":[],"read":[]}', '17/02/2017 - 00:34:48', '16/02/2017 - 22:01:15', 'A', 'A'),
(2, 'character test', 'html', 'Hello%3F', 'cFyS7BVMGin5TK58BjIZF52oXzlaNoLx', '{"owner":"A","admin":[],"read":[]}', '16/02/2017 - 23:51:44', '16/02/2017 - 22:49:56', 'A', 'A'),
(3, 'Proba na mobitelu', 'c_cpp', '%23include%20%3Ccstdio%3E%0A%0Aint%20main%20(void)%20%7B%0A%20%20%20%20%0A%20%20%20%20printf(''%C5%BEivot%20je%20lijep'')%3B%0A%20%20%20%20%0A%20%20%20%20return%200%3B%0A%7D', 'rHpaY1290zHkgbNhTew5EcyqHThmZTez', '{"owner":"A","admin":[],"read":[]}', '16/02/2017 - 23:55:11', '16/02/2017 - 23:55:11', 'A', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `sessid` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `sessid`) VALUES
(1, 'A', 'd9db6054350cd6a2f42f92b7e620fa4490df71b64d55b48f812ab192346bfd45', '35aac3ddc5a5aebe3974c833d256fdc5df3eecda361250bff30db306a70cec05'),
(2, 'Antonio', 'd9db6054350cd6a2f42f92b7e620fa4490df71b64d55b48f812ab192346bfd45', '6ad5935af6f3bdf602f2cbfd90e085653e145b3dea46a03cc2c27e98dfbf8044');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
