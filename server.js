//	TODO(Djeno) : lot of shit needs to be done here
//								dont let node to server html to client side
//								make views in angular

"use strict";
// Importes needed modules
var express = require("express");
var bodyParser = require("body-parser");
var crypto = require("crypto");
var mysql = require("mysql");
var session = require("express-session");
var cookieParser = require("cookie-parser");
var url = require("url");
var secret = "Cloude is the best";
var app = require("express")();
var fs = require("fs");
var spawnSync = require("child_process").spawnSync;
var execSync = require("child_process").execSync;
var http = require("http").Server(app);
var io = require("socket.io")(http);
var nodemailer = require("nodemailer");

// Makes app use "static" folder so server.js can access those files
app.use(express.static(__dirname + "/static"));
app.use("/static", express.static(__dirname + "/static"));

// Set up a session so we can distinguish logged users from ordinary ones
app.use(session({
	secret: "cloude is the best",
	resave: true,
	saveUninitialized: true
}));
app.use(cookieParser());

// App is using bodyParser so server can read body of the requests from the client
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let transporter = nodemailer.createTransport({
	service: "gmail",
	auth: {
		user: "infocloude@gmail.com",
		pass: "tonidinosasa"
	}
});

// Connect to database
/* var connection = mysql.createConnection({
	host: "eu-cdbr-west-01.cleardb.com",
	user: "be83ecdca12ee6",
	password: "02d4d544",
	database: "heroku_4e77e6f4917bc97"
});
connection.connect(); */

var db_config = {
	host: "eu-cdbr-west-01.cleardb.com",
	user: "be83ecdca12ee6",
	password: "02d4d544",
	database: "heroku_4e77e6f4917bc97"
};

var connection;

function handleDisconnect() {
	connection = mysql.createConnection(db_config); // Recreate the connection, since                                                 // the old one cannot be reused.

	connection.connect(function(err) {              // The server is either down
		if(err) {                                     // or restarting (takes a while sometimes).
			console.log('[SQL] error when connecting to db:', err);
			setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
		}                                     // to avoid a hot loop, and to allow our node script to
	});                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
	connection.on('error', function(err) {
		console.log('[SQL] db error', err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
			handleDisconnect();                         // lost due to either server restart, or a
		} else {                                      // connnection idle timeout (the wait_timeout
			throw err;                                  // server variable configures this)
		}
	});
}

handleDisconnect();

for (var i = 0; i < 365; i++) {
	setInterval(function () {
		connection.query("DELETE FROM chats WHERE datetimes < NOW() - INTERVAL 1 DAY");
	}, 86400000);
}
/* DELETE FROM chats WHERE datetime < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 DAY)) */

// If port is defined by server start on it, otherwise start on port 3000
http.listen(process.env.PORT || 3000, function () {
	console.log("[Express] Listening on port 3000");
});

io.on("connection", function(socket){
	socket.on("chat message", function (msg, room) {
		//var activeUsers = io.nsps['/'].adapter.rooms[room];
		io.of("chat message" + room).clients(function(error, clients){
			if (error) throw error;
			console.log(clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
		});
		console.log("+-------------------------------+");
		console.log("room: \"" + room + "\"");
		io.emit("chat message" + room, msg);
		console.log(msg);

		//console.log(sender, msg);

		connection.query(`INSERT INTO chats VALUES (null, "${room}", "${msg}", "${new Date().toMysqlFormat()}")`, function (err, rows, fields) {
			// Error handling
			if (err) {
				if (err.code == "ER_DUP_ENTRY") {
					response.status(200).send("ER_DUP_ENTRY");
				} else if (err) {
					console.log("[SQL] io.connection POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
			}
			
		});
		
		//console.log(msg);
		//console.log("Users:", activeUsers);
		console.log("+-------------------------------+");
	});
});

// Start page, if logged redirect to home, else show signin.html
app.get("/login", function (request, response) {
	if (request.session.sessid) response.redirect("/home");
	else response.sendFile(__dirname + "/static/html/signin.html");
});

// Home page which is shown to the logged users
app.get("/home", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/home.html");
	else response.redirect("/");
});

// Shared page which shows files shared with the user
app.get("/shared", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/shared.html");
	else response.redirect("/");
});

app.get("/chat", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/chat.html");
	else response.redirect("/");
});

app.get("/forgot", function (request, response) {
	if (request.session.sessid) response.redirect("/");
	else response.sendFile(__dirname + "/static/html/forgot.html");
});

app.get("/preferences", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/preferences.html");
	else response.redirect("/");
});

app.get("/newhtml", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/newhtmlproject.html");
	else response.redirect("/");
});

app.get("/public", function (request, response) {
	response.sendFile(__dirname + "/static/html/public.html")
})

// This post checks are entered username and password valid
app.post("/signin", function (request, response) {
	var username = request.body.username;
	var password = request.body.password;
	
	// Send query to the database
	connection.query(`SELECT * FROM users WHERE BINARY username="${username}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/signin POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		// If username is undefined then that username doesn't exist in db
		if (typeof rows[0] === "undefined") {
			response.status(200).send("956");
		} else {
			// Get encrypted password from db and compare it with the one user has entered
			var dbPassword = rows[0]["password"];
			var dbSessid = rows[0]["sessid"];
			var hash = crypto.createHmac("sha256", secret).update(password).digest("hex");
			if (hash == dbPassword) {
				request.session.sessid = dbSessid;
				request.session.username = username;
				response.status(200).send("success");
			} else {
				response.status(200).send("924");
			}
		}
	});
	
});

app.post("/forgotPassword", function (request, response) {
	var email = request.body.email;
	var id = generateHash(32);
	"use strict";
	let mailOptions = {
		from: '"Cloude Team ©" <infocloude@gmail.com>', // sender address
		to: email, // list of receivers
		subject: "Forgot password request", // Subject line
		text: 'Hello, it seems you have forgot your password. Follow this link to regain access to your account: http://cloude.tk/forgot?id=' + id/* , // plain text body
		html: '<b>Hello world ?</b>' // html body */
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			return console.log(error);
		}
		console.log('Message %s sent: %s', info.messageId, info.response);
		connection.query(`INSERT INTO forgot VALUES (null, "${id}" , "${email}")`, function (err, rows, fields) {
			// Error handling
			if (err) {
				return err;
			} else {
				// When all successfully finished, redirect to homepage
				response.status(200).send("success");
			}
			
		});
	});
});

app.post("/newForgotPassword", function (request, response) {
	var password1 = request.body.password1;
	var password2 = request.body.password2;
	var unique_id = request.body.unique_id;

	console.log(password1, password2, unique_id);
	connection.query(`SELECT * FROM forgot WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/newForgotPassword POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		console.log(rows[0]);
		if (rows[0]) {
			var email = rows[0]["email"];
			var newPassword = crypto.createHmac("sha256", secret).update(password1).digest("hex");
			connection.query('UPDATE users SET ? WHERE ?', [{ password: newPassword }, { email: email }], function (err) {
				// Error handling
				if (err) {
					console.log("/newForgotPassword POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
				connection.query(`DELETE FROM forgot WHERE email='${email}';`, function (err) {
					if (err) {
						console.log("/newForgotPassword POST query ERROR", err);
						response.sendFile(__dirname + "/static/html/error/404.html");
						return err;
					}

					response.status(200).send("OK");
				});

			});
		} else {
			response.status(401).send("Authorization error");
		}

	});

});

// This post validates given info and creates new user in the database
app.post("/signup", function (request, response) {
	var username = request.body.usernameUp;
	var password = request.body.passwordUp;
	var password2 = request.body.passwordUp2;
	var email = request.body.emailUp;
	console.log(email);

	// If two passwords are different return back
	if (password != password2) {
		response.sendFile(__dirname + "/static/html/error/924.html");
	} else if (username.length <= 25) { // Max number of characters of username in db is 25

		var hash = crypto.createHmac("sha256", secret).update(password).digest("hex");
		var hash2 = crypto.createHmac("sha256", secret).update(username).digest("hex");
		var empty = "[]";
		var theme = "xcode";
		// Send query to database and insert these values
		connection.query(`INSERT INTO users VALUES (null, "${email}" , "${username}", "${hash}", "${hash2}", "${empty}", 1, "${theme}", "green", 0)`, function (err, rows, fields) {
			// Error handling
			if (err) {
				if (err.code == "ER_DUP_ENTRY") {
					response.status(200).send("ER_DUP_ENTRY");
				} else if (err) {
					console.log("[SQL] /signup POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
			} else {
				// When all successfully finished, redirect to homepage
				response.status(200).send("success");
			}
			
		});
		
	} else { // This else will activate if username has more than 25 characters
		response.sendFile(__dirname + "/static/html/error/924.html");
	}
});

// When user goes to this route all sessions are destroyed and user is logged out
app.get("/signout", function (request, response) {
	request.session.destroy();
	response.redirect("/");
});

app.get("/change", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/change.html");
	else response.redirect("/");
});

app.get("/new", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/new.html");
	else response.redirect("/");
});

app.get("/publicFile", function (request, response) {
	response.sendFile(__dirname + "/static/html/publicFile.html");
});

app.get("/", function (request, response) {
	if (request.session.sessid) response.redirect("/home");
	else response.sendFile(__dirname + "/static/html/index.html");
});

app.get("/file", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/file.html");
	else response.redirect("/");
});

app.get("/list", function (request, response) {
	if (request.session.sessid) response.sendFile(__dirname + "/static/html/list.html");
	else response.redirect("/");
});

app.post("/returnPublicShared", function (request, response) {
	connection.query(`SELECT value FROM files WHERE unique_id="public";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/returnPublicShared POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		var value = rows[0]["value"];

		var ids = value.split(",");

		response.status(200).json(ids);
	});
	
});

app.post("/fileInfo", function (request, response) {
	var unique_id = request.body.unique_id;
	connection.query(`SELECT * FROM files WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/fileInfo POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		response.status(200).json(rows[0]);

	});
	
});

app.post("/readFile", function (request, response) {
	var unique_id = request.body.unique_id;

	connection.query(`SELECT * FROM files WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/readFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		var users = rows[0]["users"];
		users = JSON.parse(users);

		var file = {};

		file = {id: rows[0]["id"], title: rows[0]["title"], extension: rows[0]["extension"], value: rows[0]["value"]};
		
		response.status(200).json(file);
	});
	
});

app.post("/compile", function(request, response) {
	//console.log(request);
	var kod = request.body.kod;
	var ins = request.body.ins;
	fs.writeFile("main.cpp", kod + "\n" , (err) => {
		if (err) {
			throw err;
		}

		var error = "";
		try {
			execSync("g++ main.cpp -o a.out -lm");
		} catch (err) {
			error = err.stderr.toString();
		}
		var output = spawnSync("./a.out", {timeout : 250, input: ins});
		response.json({output: output.stdout.toString(), error: error});
	});
});

/* SECURE ROUTE *** START *** */

var router = express.Router();

router.use(function (request, response, next) {
	if (request.session.username) var username = request.session.username;
	else var username = "";

	if (request.session.sessid) var sessid = request.session.sessid;
	else var sessid = "";

	var hash = crypto.createHmac("sha256", secret).update(username).digest("hex");
	if (hash == sessid) {
		next();
	} else {
		response.status(401).json({Authorization: "You do not have permission!"});
	}
});

router.post("/info", function (request, response) {
	var username = request.session.username;
	var sessid = request.session.sessid;
	connection.query(`SELECT * FROM users WHERE sessid="${sessid}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/fileInfo POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		var acceptedCookies = rows[0]["acceptedCookies"];
		var responseJson = {username: username, sessid: sessid, acceptedCookies: acceptedCookies};
		response.send(responseJson);

	});
	
});

router.post("/preferencesInfo", function (request, response) {
	var sessid = request.session.sessid;
	connection.query(`SELECT * FROM users WHERE sessid="${sessid}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/fileInfo POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		var jsEditor = rows[0]["jsEditor"];
		var editorTheme = rows[0]["editorTheme"];
		var theme = rows[0]["theme"];
		response.status(200).json({jsEditor: jsEditor, editorTheme: editorTheme, theme: theme});

	});
	
});

router.post("/jsEditorToggle", function (request, response) {
	var sessid = request.session.sessid;
	var state = request.body.state;

	connection.query('UPDATE users SET ? WHERE ?', [{ jsEditor: state }, { sessid: sessid }], function (err) {
		// Error handling
		if (err) {
			console.log("/jsEditorToggle POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		response.status(200).send("OK");
	});
	
	
});

router.post("/editorThemeChange", function (request, response) {
	var sessid = request.session.sessid;
	var newTheme = request.body.newTheme;

	connection.query('UPDATE users SET ? WHERE ?', [{ editorTheme: newTheme }, { sessid: sessid }], function (err) {
		// Error handling
		if (err) {
			console.log("/editorThemeChange POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		response.status(200).send("OK");
	});
});

router.post("/acceptCookies", function (request, response) {
	var sessid = request.session.sessid;

	connection.query('UPDATE users SET ? WHERE ?', [{ acceptedCookies: 1 }, { sessid: sessid }], function (err) {
		// Error handling
		if (err) {
			console.log("/acceptCookies POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		response.status(200).send("OK");
	});
});

router.post("/createFile", function (request, response) {
	var owner = request.session.username;
	var title = request.body.title;
	var extension = request.body.extension;
	var value = request.body.value;
	var unique_id = generateHash(32);
	var users = mysql_real_escape_string(JSON.stringify({owner: owner, admin: [], write: [], read: []}));
	var timestamp = getDateTime();
	var type = "file";
	connection.query(`INSERT INTO files VALUES (null, "${title}", "${extension}", "${value}", "${unique_id}", "${users}", "${timestamp}", "${timestamp}", "${owner}", "${owner}", "${type}")`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("[SQL] /createFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		response.json({id: unique_id});
	});
	
});

router.post("/compile", function(request, response) {
	//console.log(request);
	var kod = request.body.kod;
	var ins = request.body.ins;
	fs.writeFile("main.cpp", kod + "\n" , (err) => {
		if (err) {
			throw err;
		}

		var error = "";
		try {
			execSync("g++ main.cpp -o a.out -lm");
		} catch (err) {
			error = err.stderr.toString();
		}
		var output = spawnSync("./a.out", {timeout : 250, input: ins});
		response.json({output: output.stdout.toString(), error: error});
	});
});

router.post("/createFolder", function (request, response) {
	var owner = request.session.username;
	var title = request.body.folderName;
	var extension = "";
	var value = "";
	var unique_id = generateHash(32);
	var users = mysql_real_escape_string(JSON.stringify({owner: owner, admin: [], write: [], read: []}));
	var timestamp = getDateTime();
	var type = "folder";
	connection.query(`INSERT INTO files VALUES (null, "${title}", "${extension}", "${value}", "${unique_id}", "${users}", "${timestamp}", "${timestamp}", "${owner}", "${owner}", "${type}")`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/createFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		response.json({id: unique_id});
	});
	
});

router.post("/updateFile", function (request, response) {
	var user = request.session.username;
	var title = request.body.title;
	var extension = request.body.extension;
	var value = request.body.value;
	var unique_id = request.body.unique_id;
	var timestamp = getDateTime();
	//connection.query(`UPDATE files SET title="${title}", extension="${extension}", value="${value}", last_modified_date="${timestamp}", last_modified_user="${user}" WHERE unique_id="${unique_id}")`, function (err) {
	connection.query('UPDATE files SET ?, ?, ?, ?, ? WHERE ?', [{ title: title }, { extension: extension }, { value: value }, { last_modified_date: timestamp }, { last_modified_user: user }, { unique_id: unique_id }], function (err) {
		// Error handling
		if (err) {
			console.log("/updateFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		response.json({id: unique_id});
	});
	
});

router.post("/change", function (request, response) {
	var username = request.session.username;
	var oldPassword = request.body.oldPassword;
	var newPassword = request.body.newPassword;
	connection.query(`SELECT password FROM users WHERE BINARY username="${username}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/signin POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		var dbPassword = rows[0]["password"];
		var hash = crypto.createHmac("sha256", secret).update(oldPassword).digest("hex");
		if (hash == dbPassword) {
			var newHash = crypto.createHmac("sha256", secret).update(newPassword).digest("hex");
			connection.query('UPDATE users SET ? WHERE BINARY ?', [{ password: newHash }, { username: username }], function (err) {
				// Error handling
				if (err) {
					console.log("/moveFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}

				response.status(200).send("success");
			});

		} else {
			response.status(200).send("924");
		}
	});
	
});

router.post("/renameFile", function (request, response) {
	var unique_id = request.body.unique_id;
	var title = request.body.title;

	connection.query('UPDATE files SET ? WHERE ?', [{ title: title }, { unique_id: unique_id }], function (err) {
		// Error handling
		if (err) {
			console.log("/renameFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		response.status(200).send("OK");
	});
	
});

router.post("/moveFile", function (request, response) {
	var username = request.session.username;
	var unique_id = request.body.unique_id;
	var selectedFolder = request.body.selectedFolder;
	var currentFolder = request.body.currentFolder;

	if (selectedFolder == "" && currentFolder != "") {
		connection.query(`SELECT value FROM files WHERE unique_id="${currentFolder}";`, function (err, rows, fields) {
			// Error handling
			if (err) {
				console.log("/moveFile POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}
			var files = rows[0]["value"];
			files = files.replace(unique_id + ",", "");

			connection.query('UPDATE files SET ? WHERE ?', [{ value: files }, { unique_id: currentFolder }], function (err) {
				// Error handling
				if (err) {
					console.log("/moveFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}

				response.status(200).send("OK");
			});
		});
		
	} else if (selectedFolder != "" && currentFolder == "") {
		connection.query(`SELECT value FROM files WHERE unique_id="${selectedFolder}";`, function (err, rows, fields) {
			// Error handling
			if (err) {
				console.log("/moveFile POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}
			var files = rows[0]["value"];
			files += unique_id + ",";

			connection.query('UPDATE files SET ? WHERE ?', [{ value: files }, { unique_id: selectedFolder }], function (err) {
				// Error handling
				if (err) {
					console.log("/moveFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}

				response.status(200).send("OK");
			});
		});
		
	} else if (selectedFolder != "" && currentFolder != "") {
		connection.query(`SELECT value FROM files WHERE unique_id="${currentFolder}";`, function (err, rows, fields) {
			// Error handling
			if (err) {
				console.log("/moveFile POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}
			var files = rows[0]["value"];
			files = files.replace(unique_id + ",", "");

			connection.query('UPDATE files SET ? WHERE ?', [{ value: files }, { unique_id: currentFolder }], function (err) {
				// Error handling
				if (err) {
					console.log("/moveFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}

				connection.query(`SELECT value FROM files WHERE unique_id="${selectedFolder}";`, function (err, rows, fields) {
					// Error handling
					if (err) {
						console.log("/moveFile POST query ERROR", err);
						response.sendFile(__dirname + "/static/html/error/404.html");
						return err;
					}
					var files = rows[0]["value"];
					files += unique_id + ",";

					connection.query('UPDATE files SET ? WHERE ?', [{ value: files }, { unique_id: selectedFolder }], function (err) {
						// Error handling
						if (err) {
							console.log("/moveFile POST query ERROR", err);
							response.sendFile(__dirname + "/static/html/error/404.html");
							return err;
						}

						response.status(200).send("OK");
					});
				});
			});
		});
		
	}
	
});

router.post("/readFile", function (request, response) {
	var username = request.session.username;
	var unique_id = request.body.unique_id;
	var permission = "none";

	connection.query(`SELECT * FROM files WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/readFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		var users = rows[0]["users"];
		users = JSON.parse(users);

		var file = {};

		if (users["owner"] == username) permission = "owner";
		else if (users["admin"].indexOf(username) > -1) permission = "admin";
		else if (users["write"].indexOf(username) > -1) permission = "write";
		else if (users["read"].indexOf(username) > -1) permission = "read";

		if (permission != "none") file = {id: rows[0]["id"], title: rows[0]["title"], extension: rows[0]["extension"], value: rows[0]["value"], permission: permission};
		else file = {permission: permission};
		
		response.status(200).json(file);
	});
	

});

router.post("/readPublicFile", function (request, response) {
	var unique_id = request.body.unique_id;
	var permission = "none";


	connection.query(`SELECT value FROM files WHERE unique_id="public";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/returnPublicShared POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		var value = rows[0]["value"];

		var ids = value.split(",");

		if (ids.indexOf(unique_id) > -1) {
			connection.query(`SELECT * FROM files WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
				// Error handling
				if (err) {
					console.log("/readFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
				var users = rows[0]["users"];
				users = JSON.parse(users);

				var file = {};

				permission = "public";

				if (permission != "none") file = {id: rows[0]["id"], title: rows[0]["title"], extension: rows[0]["extension"], value: rows[0]["value"], permission: permission};
				else file = {permission: permission};
				
				response.status(200).json(file);
			});
		} else {
			response.status(401).json({Authorization: "You do not have permission!"});
		}

		
	});
	
});

router.post("/returnList", function (request, response) {
	var username = request.session.username;
	connection.query(`SELECT * FROM files WHERE owner="${username}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/returnList POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		response.status(200).json(rows);
	});
	
});

router.post("/returnShared", function (request, response) {
	var username = request.session.username;
	connection.query(`SELECT shared FROM users WHERE username="${username}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/returnShared POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		
		var shared = rows[0]["shared"];

		var str = JSON.stringify(shared);
		shared = JSON.parse(str);

		sharedObject = [];
		for (var event in shared) {
			var dataCopy = shared[event];
			for (var key in dataCopy) {
				if (key == "start" || key == "end") {
					// needs more specific method to manipulate date to your needs
					dataCopy[key] = new Date(dataCopy[key]);
				}
			}
			sharedObject.push(dataCopy);
		}

		var objj = "";
		for (var i in sharedObject) {
			objj += sharedObject[i];
		}

		var sharedObject = JSON.parse(objj);

		response.status(200).json(sharedObject);
	});
	
});

router.post("/fileInfo", function (request, response) {
	var username = request.session.username;
	var unique_id = request.body.unique_id;
	connection.query(`SELECT * FROM files WHERE unique_id="${unique_id}";`, function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/fileInfo POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		if (rows[0]) {
			var users = JSON.parse(rows[0]["users"]);
			var owner = users.owner;
			var admin = users.admin;
			var write = users.write;
			var read = users.read;
			var existing = 0;

			if (owner == username) existing = 1;
			else if (admin.indexOf(username) > -1) existing = 1;
			else if (write.indexOf(username) > -1) existing = 1;
			else if (read.indexOf(username) > -1) existing = 1;
			
			if (existing == 1) response.status(200).json(rows[0]);
			else response.status(401).json({Authorization: "You do not have permission!"});
		} else {
			response.status(401).json({Authorization: "You do not have permission!"});
		}
	});
	
});

router.post("/deleteFile", function (request, response) {
	var unique_id = request.body.unique_id;

	connection.query(`DELETE FROM files WHERE unique_id='${unique_id}';`, function (err) {
		if (err) {
			console.log("/deleteFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		response.status(200).json({success: true});
	});
	
});

router.post("/getRoomMessages", function (request, response) {
	var room = request.body.room;

	connection.query("SELECT * FROM chats WHERE room='" + room + "';", function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/getRoomMessages POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		for (var i = 0; i < rows.length; i++) {
			rows[i]["datetimes"] = rows[i]["datetimes"].toString();
		}
		
		response.status(200).json(rows);
	});
	

});

router.post("/deleteFolder", function (request, response) {
	var unique_id = request.body.unique_id;
	connection.query("SELECT value FROM files WHERE unique_id='" + unique_id + "';", function (err, rows, fields) {
		// Error handling
		if (err) {
			console.log("/deleteFolder1 POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}
		var value = rows[0]["value"];

		var ids = value.split(",");
		ids.pop();

		for (var i = 0; i < ids.length; i++) {
			connection.query(`DELETE FROM files WHERE unique_id='${ids[i]}';`, function (err) {
				if (err) {
					console.log("/deleteFolder2 POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
			});
		}
		connection.query(`DELETE FROM files WHERE unique_id='${unique_id}';`, function (err) {
			if (err) {
				console.log("/deleteFolder3 POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}

			response.status(200).json({success: true});
		});
	});
	
});

router.post("/unshareFile", function (request, response) {
	var username = request.session.username;
	var unique_id = request.body.unique_id;
	connection.query("SELECT shared FROM users WHERE username='" + username + "';", function (err, rows) {
		if (err) {
			console.log("/unshareFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		var users = rows[0]["shared"];

		var str = JSON.stringify(users);
		users = JSON.parse(str);

		usersObject = [];
		for(var event in users){
			var dataCopy = users[event];
			for(key in dataCopy){
				if(key == "start" || key == "end"){
					// needs more specific method to manipulate date to your needs
					dataCopy[key] = new Date(dataCopy[key]);
				}
			}
		usersObject.push(dataCopy);
		}

		var obj1 = "";
		for (var i in usersObject) {
			obj1 += usersObject[i];
		}

		var obj2 = JSON.parse(obj1);
		removeFromArray(obj2, unique_id);
		connection.query('UPDATE users SET ? WHERE ?', [{ shared: JSON.stringify(obj2) }, { username: username }], function (err) {
			// Error handling
			if (err) {
				console.log("/unshareFile POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}

			response.status(200).send("OK");
		});
	});
	
});

router.post("/shareFile", function (request, response) {
	var unique_id = request.body.unique_id;
	var username = request.body.username;
	var permission = request.body.permission;

	connection.query("SELECT users FROM files WHERE unique_id='" + unique_id + "';", function (err, rows) {
		if (err) {
			console.log("/shareFile POST query ERROR", err);
			response.sendFile(__dirname + "/static/html/error/404.html");
			return err;
		}

		var users = rows[0]["users"];

		var str = JSON.stringify(users);
		users = JSON.parse(str);

		var usersObject = [];
		for(var event in users){
			var dataCopy = users[event];
			for (var key in dataCopy){
				if (key == "start" || key == "end"){
					// needs more specific method to manipulate date to your needs
					dataCopy[key] = new Date(dataCopy[key]);
				}
			}
		usersObject.push(dataCopy);
		}

		var obj1 = "";
		for (var i in usersObject) {
			obj1 += usersObject[i];
		}

		var obj2 = JSON.parse(obj1);

		if (permission == "Admin") {
			obj2["admin"].push(username);
		} else if (permission == "Write") {
			obj2["write"].push(username);
		} else if (permission == "Read") {
			obj2["read"].push(username);
		}

		connection.query('UPDATE files SET ? WHERE ?', [{ users: JSON.stringify(obj2) }, { unique_id: unique_id }], function (err) {
			// Error handling
			if (err) {
				console.log("/shareFile POST query ERROR", err);
				response.sendFile(__dirname + "/static/html/error/404.html");
				return err;
			}

			connection.query(`SELECT shared FROM users WHERE username="${username}";`, function (err, rows, fields) {
				// Error handling
				if (err) {
					console.log("/shareFile POST query ERROR", err);
					response.sendFile(__dirname + "/static/html/error/404.html");
					return err;
				}
				
				//response.status(200).json(rows);
				var shared = rows[0]["shared"];

				var str = JSON.stringify(shared);
				shared = JSON.parse(str);

				var sharedObject = [];
				for(var event in shared){
					var dataCopy = shared[event];
					for (var key in dataCopy){
						if (key == "start" || key == "end"){
							// needs more specific method to manipulate date to your needs
							dataCopy[key] = new Date(dataCopy[key]);
						}
					}
				sharedObject.push(dataCopy);
				}

				var objj = "";
				for (var i in sharedObject) {
					objj += sharedObject[i];
				}

				var objj2 = JSON.parse(objj);

				if (objj2.indexOf(unique_id) < 0){
					objj2.push(unique_id);
				}

				connection.query('UPDATE users SET ? WHERE ?', [{ shared: JSON.stringify(objj2) }, { username: username }], function (err) {
					// Error handling
					if (err) {
						console.log("/shareFile POST query ERROR", err);
						response.sendFile(__dirname + "/static/html/error/404.html");
						return err;
					}

					response.status(200).send("OK");

				});

			});

		});
		
	});


});

// When trying to access subroute of /api use router
app.use("/api", router);

// If route is not listed above return 404 error
app.get("*", function (request, response) {
	response.status(404).sendFile(__dirname + "/static/html/error/404.html");
});

/* SECURE ROUTE *** END *** */

// Functions

function generateHash(length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < length; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}

function getDateTime() {

	var date = new Date();

	var hour = date.getHours();
	hour = (hour < 10 ? "0" : "") + hour;

	var min  = date.getMinutes();
	min = (min < 10 ? "0" : "") + min;

	var sec  = date.getSeconds();
	sec = (sec < 10 ? "0" : "") + sec;

	var year = date.getFullYear();

	var month = date.getMonth() + 1;
	month = (month < 10 ? "0" : "") + month;

	var day  = date.getDate();
	day = (day < 10 ? "0" : "") + day;

	return day + "/" + month + "/" + year + " - " + hour + ":" + min + ":" + sec;

}

function removeFromArray(arr) {
	var what, a = arguments, L = a.length, ax;
	while (L > 1 && arr.length) {
		what = a[--L];
		while ((ax= arr.indexOf(what)) !== -1) {
			arr.splice(ax, 1);
		}
	}
	return arr;
}

function mysql_real_escape_string (str) {
	return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
		switch (char) {
			case "\0":
				return "\\0";
			case "\x08":
				return "\\b";
			case "\x09":
				return "\\t";
			case "\x1a":
				return "\\z";
			case "\n":
				return "\\n";
			case "\r":
				return "\\r";
			case "\"":
			case "'":
			case "\\":
			case "%":
				return "\\"+char; // prepends a backslash to backslash, percent,
								  // and double/single quotes
		}
	});
}

function twoDigits(d) {
	if(0 <= d && d < 10) return "0" + d.toString();
	if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	return d.toString();
}

Date.prototype.toMysqlFormat = function() {
	return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};